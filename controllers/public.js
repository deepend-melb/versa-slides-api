const express = require("express");
const helpers = require("../helpers");
const router = new express.Router();

const commands = {
  next: `xdotool key --window "$(xdotool search "Slides" | head -n1)" Right`,
  previous: `xdotool key --window "$(xdotool search "Slides" | head -n1)" Left`,
  fullScreen:
    'xdotool key --window "$(xdotool search "Slides" | head -n1)" ctrl+F5',
  exitFullScreen:
    'xdotool key --window "$(xdotool search "Slides" | head -n1)" Escape',
  goToSlide: slideNumber =>
    `xdotool key --window "$(xdotool search "Slides" | head -n1)" ${slideNumber} Return`
};

router.post("/controller", async (req, res) => {
  const { command } = req.body;

  // request validation
  if (typeof command !== "string") {
    return res.send("Command argument must be a string.");
  }
  if (!commands[command] && !command.includes('goToSlide')) {
    return res.send(
      `Command not supported. Available commands: ${Object.keys(commands)}`
    );
  }

  if (command.includes("goToSlide")) {
    const slideNumber = command.replace("goToSlide_", "");
    await helpers.execute(commands.goToSlide(slideNumber));
  } else {
    await helpers.execute(commands[command]);
  }

  // return res.send(commands[command]);
  return res.send(`${command} => executed`);
});

router.get("/", (req, res) => {
  return res.send(`Available commands: ${Object.keys(commands)}. If you are going to use goToSlide remember to specify the number. eg. goToSlide_3`);
});
module.exports = router;
