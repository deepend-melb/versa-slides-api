const exec = require("child_process").exec;
const fs = require("fs");

exports.execute = command => {
  return new Promise((resolve, reject) => {
    const script = exec(command);
    console.log({ executingCommand: command });

    script.stdout.on("data", data => {
      console.log(data);
    });

    // This is disabled because when ask-cli asks for confirmation is a warn so the promise gets rejected:
    script.stderr.on("data", err => {
      console.log(err);
    });

    script.on("close", () => {
      resolve("done");
    });
  });
};

module.isValidCommand = command =>{
  if (typeof command!=='string'){
    return false
  }
  return true
}