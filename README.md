## Description:
This is a simple express server that allows remote control of the hosting machine's UI, by exposing the '/controller' endpoint and executing the requested command with `xdotool`.

Please note that the current version only supports Unix-like operating systems (eg. macOS, Ubuntu)

### Hosting Machine Requirements:
* node v.10
* xdotool

### Setup Instructions: 
* `npm  install`
* `npm start`

### How to use
After the server is installed and configured any device can control the hosting machine by sending an `application/json` post request to the following route:

`$your-network-ip/controller`

The request should have the following structure
```json
{
    "command":"your-command"
}
```

If you don't know your network IP you can run `hostnames -I`.

To get a list of the supported commands just open your ngrok endpoint in the browser.
`$your-network-ip/`

## Develop
This server uses xdotool to control the hosting machine UI. An example of a xdotool command would be:
`xdotool key --window "$(xdotool search "Slides" | head -n1)" Right`

This command will find the window containing "Slides" in the title and then press the "Right" key. You can add more commands in the controllers/public.js file. If you want to add a key but you are not sure of the correct way to call it with xdotool try running `xev` on the terminal. 
