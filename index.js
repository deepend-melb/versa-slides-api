const express = require("express");

const app = express();
const port = process.env.PORT || 5000;

//by doing this here we are telling express to use express.json() in all the app.
app.use(express.json());
//we are telling app to use everything in the controllers folder.
app.use(require("./controllers"));

app.listen(port, () => {
  console.log(`listening on ${port}`);
});
